package Controller;

import Model.Circle;
import Model.Rectangle;
import Model.Triangle;
import java.io.Console;
import static java.lang.System.exit;
import java.util.Scanner;

public class Choose {
    Circle circle;
    Triangle triangle;
    Rectangle rectangle;
    Console cnsl = null;
    String input = null;
    Scanner scanner =null;
    Scanner scannerTemp = null;
    public Choose(){
        //scanner = new Scanner(System.in);
    }
    public void start(){
        try{
                System.out.print("Choose from 1-Circle, 2-Triangle, 3-Rectangle, 0-Exit: ");
                scanner = new Scanner(System.in);
                input = scanner.nextLine();
                if (scanner != null) {
                switch(input){
                    case "0" : exit(0);
                    case "1" : circleCalculator();break;
                    case "2" : triangleCalculator();break;
                    case "3" : rectangleCalculator();break;
                    default: System.out.println("WRONG CHOOSE !");break;
                }
                }
        }catch(Exception e){
            System.err.println(e);
        }
    }
    private void circleCalculator(){
        double radius;
        try{
            System.out.print("Introduce the radius: ");
            radius = scanner.nextDouble();
            if(radius<0)
                System.out.println("You did introduce a negative number !");
            else
            {
                circle = new Circle(radius);
                System.out.println("The area is "+circle.getArea());
                System.out.println("The perimeter is "+circle.getPerimeter());
            }
        }catch(Exception e){
            System.err.println("WRONG INPUT!");
        }finally{
            try{
                scanner.close();
            }catch(Exception e){
                System.err.println(e);
            }
        }
    }
    private void triangleCalculator(){
        double side1, side2, side3;
        triangle = new Triangle();
        try{
            System.out.print("Introduce the first side: ");
            side1 = scanner.nextDouble();
            System.out.print("Introduce the second side: ");
            side2 = scanner.nextDouble();
            System.out.print("Introduce the third side: ");
            side3 = scanner.nextDouble();
            if(side1 >0 && side2>0 && side3>0 && triangle.sidesVerification(side1, side2, side3))                
            {
                triangle = new Triangle(side1,side2,side3);
                System.out.println("The area is "+triangle.getArea());
                System.out.println("The perimeter is "+triangle.getPerimeter());
            }
            else
                System.out.println("You did introduce invalid numbers !");
        }catch(Exception e){
            System.out.println("WRONG INPUT!");
        }finally{
            try{
                scanner.close();
            }catch(Exception e){
                System.err.println(e);
            }
        }
    }
    private void rectangleCalculator(){
        double side1, side2;
        try{
            System.out.print("Introduce a side: ");
            side1 = scanner.nextDouble();
            System.out.print("Introduce other side: ");
            side2 = scanner.nextDouble();
            if(side1 < 0 || side2<0)
                System.out.println("You did introduce negative numbers !");
            else
            {
                rectangle = new Rectangle(side1,side2);
                System.out.println("The area is "+rectangle.getArea());
                System.out.println("The perimeter is "+rectangle.getPerimeter());
            }
        }catch(Exception e){
            System.err.println("WRONG INPUT!");
        }finally{
            try{
                scanner.close();
            }catch(Exception e){
                System.err.println(e);
            }
        }
    }
}
