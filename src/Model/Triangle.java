package Model;

public class Triangle implements Figure{
    
    private double side1;
    private double side2;
    private double side3;

    public Triangle(){}
    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public Boolean sidesVerification(double side1, double side2, double side3){
        if(side1+side2<side3 || side1+side3<side2 || side2+side3<side1)
            return false;
        return true;
    }

    @Override
    public double getArea() {
        double p = getPerimeter()/2;
        return Math.sqrt(p*(p-side1)*(p-side2)*(p-side3));
    }

    @Override
    public double getPerimeter() {
        return side1+side2+side3;
    }
}
