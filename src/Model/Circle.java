package Model;

public class Circle implements Figure{

    private double radius;
    
    public Circle(){}
    public Circle(double radius) {
            this.radius = radius;
    }

    public double getR() {
        return radius;
    }

    public void setR(double radius) {
        this.radius = radius;
    }
    
    @Override
    public double getArea(){
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
}
