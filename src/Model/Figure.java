package Model;

public interface Figure {
    
    public double getArea();
    public double getPerimeter();
}
